package com.example.cswpractice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CswPracticeApplication {

    public static void main(String[] args) {
        SpringApplication.run(CswPracticeApplication.class, args);
    }

}
