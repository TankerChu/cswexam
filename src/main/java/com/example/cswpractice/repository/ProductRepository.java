package com.example.cswpractice.repository;

import com.example.cswpractice.entity.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Integer> {
}
